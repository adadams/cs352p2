#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "encrypt-module.h"
#include <pthread.h>
#include <semaphore.h>

// Currently testing with:
// gcc encrypt-driver.c encrypt-module.c -lpthread -o encrypt
// ./encrypt /Users/andrew_a/Downloads/encrypt-module.h out.txt log.txt

sem_t sem_inputCounterNewChar; //Semaphore used in counting input
sem_t sem_encryptNewChar; //Semaphore used in encrypting input
sem_t sem_outputCounterNewChar; //Semaphore used in counting output
sem_t sem_inputCounterNewSlot; //Semaphore used in counting input
sem_t sem_encryptNewSlot; //Semaphore used in encrypting input
sem_t sem_outputCounterNewSlot; //Semaphore used in counting output
sem_t sem_outputNewSlot; //Semaphore used in output
sem_t sem_outputNewChar; //Semaphore used in output
int* inputBuffer; // char* to store characters
int* outputBuffer; // char* to store characters

int N = 0; //Integer that holds the size of the input buffer
int M = 0; //Integer that holds the size of the output buffer

// Semaphore for reader to inputCounter: reader posts and the inputCounter waits. Reader places character in buffer and moves write pointer if there is space in the input buffer; else block. The, reader posts the semaphore. The semaphore stores the number of spaces that can be counted. InputCounter waits for the semaphore and will count up til the semaphore value is zero. The count pointer records the write pointer.

// Semaphore for inputCounter to encryptor: inputCounter posts and encryptor waits. When the inputCounter counts a char in the input buffer, it posts to a semaphore for the encryptor. The semaphore stores the number of spaces that can be counted. InputCounter waits for the semaphore and will count up til the semaphore value is zero.

// Semaphore for encryptor to outputCounter: encryptor posts and outputCounter waits. When the encryptor does the prior action, it posts to the semaphore. The semaphore keeps track of how many chars in the output buffer that can be counted from the count pointer. The outputCounter counts the chars, updates the pointer, and posts the next semaphore.

// Semaphore for outputCounter to writer: outputCounter posts and writer waits. OutputCounter posts to this semaphore so writer knows how many chars are ready to be read. The writer updates the read pointer as it writes to the output.

/**
* Ensures the module is in a safe state to proceed with a reset and logs the current character counts.
**/ 
void reset_requested() {
    // set a lock to lock reading in the inputThread
    sem_wait(&sem_inputCounterNewSlot);
    // while loop to wait each semaphore to be empty
    while(sizeof(inputBuffer) != 0 || sizeof(outputBuffer) != 0){
       if (sizeof(inputBuffer) != 0){ sem_post(&sem_inputCounterNewChar); }
	 if (sizeof(outputBuffer) != 0){ sem_post(&sem_outputCounterNewChar); }
    }   
    log_counts();
    return;
}

/**
* Resumes the reader thread after the reset completes.
**/ 
void reset_finished() {
    // unlock reading
    sem_post(&sem_inputCounterNewSlot);
}

void *inputThread(void *arg){
    int c;
    int indexInputBuffer = 0;

    while (1) { 
        sem_wait(&sem_inputCounterNewSlot);
        sem_wait(&sem_encryptNewSlot);
        c = read_input();
        // the index should be a global variable to update the write pointer of the input buffer
        inputBuffer[indexInputBuffer] = c;
        indexInputBuffer = (indexInputBuffer + 1) % N;
        sem_post(&sem_inputCounterNewChar);
        sem_post(&sem_encryptNewChar);

        if (c == EOF){
            pthread_exit(NULL);
        }
    } 
   
}

/**
* Counts occurrences of each letter in the input file. Blocks if no characters are currently 
* available in the buffer.
**/
void *inputCounter(void *arg){
    int c;
    int inputBufferCount = 0;
    // input counter does not read_input() anymore, it will read the characters in the buffer, you can define a inputBufferCount.
    while (1) { 
        sem_wait(&sem_inputCounterNewChar);
        c = inputBuffer[inputBufferCount];
        if (c == EOF){
            pthread_exit(NULL);
        }
        count_input(c);
        inputBufferCount = (inputBufferCount+ 1) % N;
        sem_post(&sem_inputCounterNewSlot);
    }
}

/**
* Encrypts a character in the input butter and places it in the output buffer.
* If no character is currently available, the thread waits until one is.
**/
void *encryptThread(void *arg){
   int c;
   int indexInputBuffer = 0;
   int indexOutputBuffer = 0;

    while (1) { 
        sem_wait(&sem_encryptNewChar);
        c = inputBuffer[indexInputBuffer];
        indexInputBuffer = (indexInputBuffer + 1) % N;
        sem_post(&sem_encryptNewSlot);

        if (c != EOF) {
            c = encrypt(c);
        }

        sem_wait(&sem_outputCounterNewSlot);
        sem_wait(&sem_outputNewSlot);
        outputBuffer[indexOutputBuffer] = c;
        indexOutputBuffer = (indexOutputBuffer + 1) % M;
        sem_post(&sem_outputCounterNewChar);
        sem_post(&sem_outputNewChar);

        if (c == EOF) {
            pthread_exit(NULL);
        }
    }
}


/**
* Counts occurrences of each letter in the output file. Blocks if no characters are currently 
* available in the buffer.
**/
void *outputCounter(void *arg){
    int c;
    int outputBufferCount = 0;

    // output counter does the same thing like input counter, you can define a outputBufferCount to update the counting pointer for the output buffer
    while (1) { 
        sem_wait(&sem_outputCounterNewChar);
        c = outputBuffer[outputBufferCount]; 
        if (c == EOF) {
            pthread_exit(NULL);
        }
        count_output(c); 
        outputBufferCount = (outputBufferCount+ 1) % M;
        sem_post(&sem_outputCounterNewSlot);
    }
}


/**
* Writes encrypted characters in the output buffer to the output file. 
* If no character is currently available, the thread blocks until one is.
**/
void *writeThread(void *arg){
    int c;
    int indexOutputBuffer = 0;

    while (1) { 
        sem_wait(&sem_outputNewChar);
        c = outputBuffer[indexOutputBuffer];
        indexOutputBuffer = (indexOutputBuffer + 1) % M;
        // get the character from output buffer not input buffer, using a outputBufferRead variable to update the output buffer read pointer
        sem_post(&sem_outputNewSlot);
        
        if (c == EOF) {
            return NULL;
        }
        write_output(c);
   }

}

/**
* Obtains the input, output, and log files as command line arguments, initializes necessary variables, and
* creates the threads used in reading, writing, and encrypting.
**/
int main(int argc, char *argv[]) {
    char *inputFile = NULL;
    char *outputFile = NULL;
    char *logFile = NULL;
   
    if( argc > 3 )
    {
        inputFile= malloc( strlen( argv[1] ) + 1 );
        if (inputFile != NULL) strcpy(inputFile, argv[1]);

        outputFile= malloc( strlen( argv[2] ) + 1 );
        if (outputFile != NULL) strcpy(outputFile, argv[2]);

        logFile= malloc( strlen( argv[3] ) + 1 );
        if (logFile != NULL) strcpy(logFile, argv[3]);
    } else {
        printf("You did not provide the correct number of arguments.\n"); 
    }

    // init(argv[1], argv[2], argv[3]);
    init(inputFile, outputFile, logFile); 

    printf("Please enter the input buffer size N: \n");
    scanf("%d", &N); 

    printf("Please enter the output buffer size M: \n");
    scanf("%d", &M);

    inputBuffer = (int*)malloc(N * sizeof(int));

    outputBuffer = (int*)malloc(M * sizeof(int));

    sem_init(&sem_inputCounterNewChar, 0, 0);
    sem_init(&sem_encryptNewChar, 0, 0);
    sem_init(&sem_inputCounterNewSlot, 0, N);
    sem_init(&sem_encryptNewSlot, 0, N);
    sem_init(&sem_outputCounterNewChar, 0, 0);
    sem_init(&sem_outputCounterNewSlot, 0, M);
    sem_init(&sem_outputNewChar, 0, 0);
    sem_init(&sem_outputNewSlot, 0, M);

    pthread_t inputThreadId;

    pthread_create(&inputThreadId, NULL, inputThread, NULL);

    pthread_t inputCounterId;

    pthread_create(&inputCounterId, NULL, inputCounter, NULL);

    pthread_t encryptId;

    pthread_create(&encryptId, NULL, encryptThread, NULL);

    pthread_t outputCounterId;

    pthread_create(&outputCounterId, NULL, outputCounter, NULL);

    pthread_t writeThreadId;

    pthread_create(&writeThreadId, NULL, writeThread, NULL);

    pthread_join(inputThreadId, NULL);
    pthread_join(inputCounterId, NULL);
    pthread_join(encryptId, NULL);
    pthread_join(outputCounterId, NULL);
    pthread_join(writeThreadId, NULL);

    printf("End of file reached.\n"); 
    log_counts();

    sem_destroy(&sem_inputCounterNewChar);
    sem_destroy(&sem_encryptNewChar);
    sem_destroy(&sem_inputCounterNewSlot);
    sem_destroy(&sem_encryptNewSlot);
    sem_destroy(&sem_outputCounterNewChar);
    sem_destroy(&sem_outputCounterNewSlot);
    sem_destroy(&sem_outputNewSlot);
    sem_destroy(&sem_outputNewChar);

    free(inputFile);
    free(outputFile);
}

